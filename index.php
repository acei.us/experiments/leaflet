<?php
if (!is_dir(dirname(__FILE__) . "/vendor/")) echo "Please install dependencies with composer.";

require_once dirname(__FILE__) . "/vendor/autoload.php";
require_once dirname(__FILE__) . "/src/Lib/MessageParser.php";

use Pecee\SimpleRouter\SimpleRouter;

// Data
define("l_webaccess", true);
define("config", json_decode(file_get_contents("config/config.json")));

$db = ParagonIE\EasyDB\Factory::create(config->database->datasourcename, config->database->databaseusername, config->database->databasepassword);
$db->run('CREATE TABLE IF NOT EXISTS "posts" (
    "id"        TEXT NOT NULL,
	"title"	    TEXT NOT NULL,
	"author"	TEXT NOT NULL,
	"body"	    TEXT NOT NULL,
	"board"	    TEXT NOT NULL,
	"posted"	INTEGER NOT NULL,
    "image",    TEXT,
    "parent"    TEXT
);');

$stats = [
    "totalposts" => $db->run("SELECT COUNT(*) as rowcount FROM posts")[0]["rowcount"],
    "totalimages" => count(scandir(config->database->imagedirectory)) - 2
];

$twig = new Twig\Environment(new Twig\Loader\FilesystemLoader('src/Templates'), [
    'cache' => false,
]);
$twig->addGlobal('config', config);
$twig->addGlobal('stats', $stats);

// Views
SimpleRouter::get("/", function () {
    global $twig;
    return $twig->render("Index.twig", [
        "parsedowncontent" => (new MessageParser())->text(file_get_contents("config/about.md")),
        "opengraph" => [
            "title" => config->name,
            "description" => "A lightweight imageboard powered by AceiusIO's Leaflet.",
            "url" => "https://" . $_SERVER["HTTP_HOST"],
            "image" => ""
        ],
    ]);
});

SimpleRouter::get("/board/{boardname}/{pagenumber?}", function ($boardname, int $pagenumber = 1) {
    global $twig, $db;

    $posts = $db->run("SELECT * FROM posts WHERE board = ? AND parent IS NULL", $boardname);
    $posts = array_reverse($posts);

    if (config->posts->pagination != false) {
        $offset = ($pagenumber - 1) * ((int) config->posts->pagination);
        $posts = array_slice($posts, $offset, (int) config->posts->pagination);
    }

    return $twig->render("Board.twig", [
        "opengraph" => [
            "title" => "$boardname - " . config->name,
            "description" => "Page $pagenumber of board $boardname",
            "url" => "http://" . $_SERVER["HTTP_HOST"] . "/board/" . $boardname,
            "image" => ""
        ],

        "board" => $boardname,
        "page" => $pagenumber,
        "posts" => $posts,
    ]);
});

SimpleRouter::get("/thread/{threadid}", function ($threadid) {
    global $twig, $db;

    $parentpost = $db->run("SELECT * FROM posts WHERE id = ?", $threadid)[0];
    $replies = $db->run("SELECT * FROM posts WHERE parent = ?", $threadid);

    return $twig->render("Thread.twig", [
        "opengraph" => [
            "title" => $parentpost["title"] . " - " . config->name,
            "description" => "Thread #$threadid in board " . $parentpost["board"],
            "url" => "http://" . $_SERVER["HTTP_HOST"] . "/thread/" . $threadid,
            "image" => "http://" . $_SERVER["HTTP_HOST"] . "/image/" . $parentpost["image"],
        ],

        "board" => $parentpost["board"],
        "page" => 0,
        "parent" => $parentpost,
        "posts" => $replies,
    ]);
});

SimpleRouter::get("/admin", function () {
    global $twig;

    return $twig->render("Admin.twig");
});

SimpleRouter::post("/admin", function () {
    global $db;
    if ($_POST["pw"] == config->adminpassword) {
        $postid = $_POST["postid"];
        switch ($_POST["action"]) {
            case "delete":
                $db->run("DELETE FROM posts WHERE id = ?", $postid);
                break;
            case "delete-img":
                $db->run("UPDATE posts SET image = null WHERE id = ?", $postid);
                break;
            default:
                # code...
                break;
        }
    }
    header("Location: /admin");
});

// API
SimpleRouter::get("/image/{filename}", function ($filename) {
    $imagepath = realpath(config->database->imagedirectory) . "/" . $filename;

    header("Content-Type: " . mime_content_type($imagepath));
    echo file_get_contents($imagepath);
});

SimpleRouter::get("/api", function () {
    header("Content-Type: application/json");

    $composerjson = json_decode(file_get_contents("composer.json"));

    return json_encode([
        "title" => "Leaflet",
        "summary" => "Leaflet imageboardboard",
        "description" => "Leaflet is a lightweight imageboard written in PHP.",
        "termsOfService" => "/",
        "license" => [
            "name" => "GNU Affero General Public License 3.0",
            "url" => "https://www.gnu.org/licenses/agpl-3.0.en.html#license-text",
        ],
        "version" => $composerjson->version,
    ]);
});

SimpleRouter::get("/api/stats", function () {
    global $stats;

    header("Content-Type: application/json");
    return json_encode($stats);
});

SimpleRouter::get("/api/board/{boardname}", function ($boardname) {
    global $db;

    header("Content-Type: application/json");
    return json_encode(array_reverse($db->run("SELECT * FROM posts WHERE board = ? AND parent IS NULL", $boardname)));
});

SimpleRouter::get("/api/thread/{threadid}", function ($threadid) {
    global $db;

    header("Content-Type: application/json");
    return json_encode($db->run("SELECT * FROM posts WHERE parent = ?", $threadid));
});

SimpleRouter::post("/api/post", function () {
    global $stats, $db;
    $parsedown = new MessageParser();

    // Check if required fields are present
    if (!isset($_POST["board"]) || !in_array($_POST["board"], config->boards)) return "Missing board paramater or desired board does not exist.";
    if (!isset($_POST["title"])) return "Missing title paramater.";
    if (!isset($_POST["body"])) return "Missing body paramater.";

    // Check for conflicting posts (Willow)
    $id = sha1($_POST["title"] . $_POST["body"] . $_POST["board"]);

    $postsWithSameId = !empty($db->run("SELECT * FROM posts WHERE id = ?", $id));
    if ($postsWithSameId) return "You cannot make duplicate posts.";

    // Handle file uploads
    $validator = new FileUpload\Validator\Simple('8M', ["image/gif", "image/png", "image/jpg", "image/jpeg", "image/webp"]);
    $pathresolver = new FileUpload\PathResolver\Simple(realpath(config->database->imagedirectory));
    $filesystem = new FileUpload\FileSystem\Simple();
    $filenamer = new FileUpload\FileNameGenerator\Slug();

    $fileupload = new FileUpload\FileUpload($_FILES['uploaded-image'], $_SERVER);
    $fileupload->setPathResolver($pathresolver);
    $fileupload->setFileSystem($filesystem);
    $fileupload->setFileNameGenerator($filenamer);
    $fileupload->addValidator($validator);

    // Doing the deed
    list($files, $headers) = $fileupload->processAll();
    if ($files[0]->completed) {
        // Strip file extension so php dev server doesnt try to serve a file
        $imagepath = ($files[0]->getRealPath());
        $extensionlessfilename = pathinfo($imagepath)['filename'];
        rename($imagepath, realpath(config->database->imagedirectory) . "/" . $extensionlessfilename);
    }

    // Create the post
    $db->insert('posts', [
        "id" => $id,
        "title" => $_POST["title"],
        "author" => "Anonymous",
        "body" => $parsedown->text($_POST["body"]),
        "board" => $_POST["board"],
        "image" => $extensionlessfilename,
        "posted" => time(),

        "parent" => isset($_POST["parent"]) ? $_POST["parent"] : null
    ]);

    // Purge old posts
    $purgeenabled = config->posts->purge != false;
    $postlimitreached = $stats["totalposts"] >= (int) config->posts->maxposts;
    if ($purgeenabled and $postlimitreached) {
        // Check if enabled, then find algorithm
        switch (config->posts->purge) {
            case "moot":
                // Moot is inspired by 4chan, it deletes the first/oldest page.
                $posts = $db->run("SELECT * FROM posts ORDER BY posted");
                for ($i = 0; $i < (int) config->posts->pagination; $i++) {
                    $victimid = $posts[$i]["id"];
                    $db->run("DELETE FROM posts WHERE id = ?", $victimid);
                }
                break;
            case "random":
                // Random picks a random quantity of random posts and deletes them
                for ($i = 0; $i < rand(0, (int) config->posts->maxposts - 1); $i++) {
                    $victimid = $db->run("SELECT * FROM posts ORDER BY posted")[$i]["id"];
                    $db->run("DELETE FROM posts WHERE id = ?", $victimid);
                }
                break;
            default:
                // Don't purge any posts, the algorithm either does not exist or is disabled.
                break;
        }
    }

    // In the future we might cache stats, so this would be the place to update them.

    echo "Redirecting...";

    if (isset($_POST["parent"])) {
        header("Location: /thread/" . $_POST["parent"]);
    } else {
        header("Location: /board/" . $_POST["board"] . "/1");
    }

    die();
});

// Start the routing
SimpleRouter::start();
