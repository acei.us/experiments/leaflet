# Leaflet
![Leaflet 2.0.0-beta1 on leaflet.acei.us](preview.png)
Project status: Complete  
Main Instance: https://leaflet.acei.us

Leaflet is a lightweight imageboard written in PHP. I created the first version as a textboard in order to learn SQLite.
Leaflet is now a full imageboard with replies, file attachments, etc.

## Features
- No cookies
- No JavaScript
- No required registration
- Runs on shared hosting
- Each page is under 10kB (excluding images)

## Contributing
Contributions are always welcome, even if you aren't a programmer. Reporting bugs is super helpful.

## Licencing
(c) 2023-2024 AceiusIO. Some rights reserved.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.